import ast.Ast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Printer {

    public static void main(String[] args) throws IOException {
        Parser yyparser;
        Ast prog;

        yyparser = new Parser(new InputStreamReader(new FileInputStream(args[0])));
        yyparser.yyparse();
        prog = (Ast) yyparser.yyval.obj;

        if (prog != null)
            System.out.println(prog);
        else
            System.out.println("Null");
    }
}
