main :-
	read(user_input, T),
	type_prog([], T, R),
	print(R).

get_type([], []).
get_type([A|ARGS], [T|TYPES]) :- type_expr([], A, T), get_type(ARGS, TYPES).

get_typeArgs([], []).
get_typeArgs([(_, T)|ARGS], [T|RES]) :- get_typeArgs(ARGS, RES).

check_args(_, [], []).
check_args(G, [ARG|ARGS],[ARGTYPE|ARGSTYPE]) :- type_expr(G, ARG, ARGTYPE), check_args(G, ARGS, ARGSTYPE).

lookup(Key, [(Key, Value)|_], Value).
lookup(Key, [_|T], Value) :- lookup(Key, T, Value).

type_expr(_, true, bool).
type_expr(_, false, bool).
type_expr(G, not(E), bool) :- type_expr(G, E, bool).
type_expr(G, eq(E1, E2), bool) :- type_expr(G, E1, int), type_expr(G, E2, int).
type_expr(G, lt(E1, E2), bool) :- type_expr(G, E1, int), type_expr(G, E2, int).
type_expr(G, add(E1, E2), int) :- type_expr(G, E1, int), type_expr(G, E2, int).
type_expr(G, sub(E1, E2), int) :- type_expr(G, E1, int), type_expr(G, E2, int).
type_expr(G, mul(E1, E2), int) :- type_expr(G, E1, int), type_expr(G, E2, int).
type_expr(G, div(E1, E2), int) :- type_expr(G, E1, int), type_expr(G, E2, int).

type_prog(G, prog([CS]), void) :- type_cmds(G, CS, void).

type_cmds(G, cmds(D, CS), void) :- type_def(G, D, GP), type_cmds(GP, CS, void).
type_cmds(G, cmds(S), void) :- type_stat(G, S, void).

type_def(G, const(X, T, E), [(X, T)|G]) :- type_expr(G, E, T).
type_def(G, function(ID, T, ARGS, BODY), CB) :- type_expr(G, BODY, T), get_typeArgs(ARGS, RES), CB = [(ID, type_func(RES, T))|G], append(ARGS, CB, CT), CB = CT.
type_def(G, funrec(ID, T, ARGS, BODY), CB) :- get_typeArgs(ARGS, RES), append(ARGS, G, CT), CTT = [(ID, type_func(RES, T)) | CT], CB = CTT, G1 = [(ID, type_func(RES, T)) | G], type_expr(G1, BODY, T).

type_stat(G, echo(E), void) :- type_expr(G, E, int).

type_expr(_, X, int) :- num(X).
type_expr(G, X, T) :- id(X), args((X, T), G).
type_expr(G, if(E, E1, E2), T) :- type_expr(G, E, bool), type_expr(G, E1, T), type_expr(G, E2, T).
type_expr(G, and(E1, E2), bool) :- type_expr(G, E1, bool), type_expr(G, E2, bool).
type_expr(G, or(E1, E2), bool) :- type_expr(G, E1, bool), type_expr(G, E2, bool).

type_expr(G, app(id(F), ARGS), TF) :- lookup(F, G, type_func(ARGSTYPE, TF)), check_args(G, ARGS, ARGSTYPE).
type_expr(G, app(func(ARGSTYPE, BODY), ARGS), TF) :- get_typeArgs(ARGSTYPE, RES), check_args(G, ARGS, RES), append(ARGSTYPE, G, CB), type_expr(CB, BODY, TF).
type_expr(G, app(app(X, Y), ARGS), TR) :- get_type(ARGS, LT), type_expr(G, app(X, Y), type_func(LT, TR)).

type_expr(G, function(ARGS, BODY), type_func(_, TF)) :- append(ARGS, G, CB), type_expr(CB, BODY, TF).