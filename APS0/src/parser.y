/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2001 Gerwin Klein <lsf@jflex.de>                          *
 * All rights reserved.                                                    *
 *                                                                         *
 * This is a modified version of the example from                          *
 *   http://www.lincom-asg.com/~rjamison/byacc/                            *
 *                                                                         *
 * Thanks to Larry Bell and Bob Jamison for suggestions and comments.      *
 *                                                                         *
 * License: BSD                                                            *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* ========================================================================== */
/* == UPMC/master/info/4I506 -- Janvier 2016/2017/2018                     == */
/* == SU/FSI/master/info/MU4IN503 -- Janvier 2020/2021/2022                == */
/* == Analyse des programmes et sémantiques                                == */
/* ========================================================================== */
/* == hello-APS Syntaxe JAVA                                               == */
/* == Fichier: parser.y                                                    == */
/* ==  Grammaire                                                           == */
/* ========================================================================== */

%{

  import java.io.*;
  import java.util.ArrayList;
  import ast.*;

%}
      
%token <ival> NUM            
%token <sval> IDENT
%token ECHO
%token LPAR RPAR             
%token LBRA RBRA
%token SEMI
%token COLON
%token COMMA
%token STAR
%token ARROW
%token CONST
%token FUN
%token REC
%token IF
%token AND
%token OR
%token BOOL
%token INT

%type <obj> expr
%type <obj> exprs
%type <obj> stat
%type <obj> cmds
%type <obj> prog
%type <obj> def
%type <obj> type
%type <obj> types
%type <obj> args
%type <obj> arg


%start prog      
%%


prog:  LBRA cmds RBRA   { $$ = new AstProg((ArrayList<Ast>)$2); }
;

cmds:
stat                    { ArrayList<Ast>r = new ArrayList<Ast>();
                          r.add((Ast)$1);
                          $$ = r;
                        }
| def SEMI cmds		{ ((ArrayList<Ast>)$3).add((Ast)$1); $$ = $3; }
;

def:
CONST IDENT type expr                       { $$ = new AstConst((String)$2,(AstType)$3,(Ast)$4); }
| FUN IDENT type LBRA args RBRA expr	    { $$ = new AstFun((String)$2,(AstType)$3,(ArrayList<AstArg>)$5,(Ast)$7); }
| FUN REC IDENT type LBRA args RBRA expr    { $$ = new AstFunRec((String)$3,(AstType)$4,(ArrayList<AstArg>)$6,(Ast)$8); }
;

type:
BOOL  				{ $$ = new AstTypeBool(); }
| INT				{ $$ = new AstTypeInt(); }
| LPAR types ARROW type RPAR	{ $$ = new AstTypeFun((ArrayList<AstType>)$2,(AstType)$4); }
;

types:
type				{ ArrayList<AstType> r = new ArrayList<AstType>();
				  r.add((AstType)$1);
				  $$ = r;
				}
| type STAR types		{ ((ArrayList<AstType>)$3).add((AstType)$1); $$ = $3; }
;

args:
arg 				{ ArrayList<AstArg> r = new ArrayList<AstArg>();
				  r.add((AstArg)$1);
				  $$ = r;
				}
| arg COMMA args		{ ((ArrayList<AstArg>)$1).add((AstArg)$3); $$ = $1; }
;

arg:
IDENT COLON type 		{ $$ = new AstArg((String)$1,(AstType)$3); }

stat:
ECHO expr               { $$ = new AstEcho((Ast)$2); }
;

expr:
  NUM                   { $$ = new AstNum($1); }
| IDENT                 { $$ = new AstId($1); }
| LPAR IF expr expr expr RPAR { $$ = new AstIf((Ast)$3,(Ast)$4,(Ast)$5); }
| LPAR AND expr expr RPAR { $$ = new AstAnd((Ast)$3,(Ast)$4); }
| LPAR OR expr expr RPAR { $$ = new AstOr((Ast)$3,(Ast)$4); }
| LPAR expr exprs RPAR  { $$ = new AstApp((Ast)$2,(ArrayList<Ast>)$3); }
| LBRA args RBRA expr   { $$ = new AstLam((ArrayList<AstArg>)$2,(Ast)$4); }
;

exprs:
  expr                  { ArrayList<Ast> r = new ArrayList<Ast>();
                          r.add((Ast)$1);
			  $$ = r; }
| exprs expr            { ((ArrayList<Ast>)$1).add((Ast)$2); $$ = $1; }
;
%%

  public Ast prog;
  
  private Yylex lexer;


  private int yylex () {
    int yyl_return = -1;
    try {
      yylval = new ParserVal(0);
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
  }


  public void yyerror (String error) {
    System.err.println ("Error: " + error);
  }


  public Parser(Reader r) {
    lexer = new Yylex(r, this);
  }
