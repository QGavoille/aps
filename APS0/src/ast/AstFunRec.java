package ast;

import java.util.ArrayList;

public class AstFunRec extends AstFun implements Ast{
    public AstFunRec(String name, AstType type, ArrayList<AstArg> args, Ast body) {
        super(name, type, args, body);
    }

    public String toPrologString() {
        String argsString = "";
        for (AstArg arg : args) {
            argsString += arg.toPrologString() + ",";
        }
        return "funrec(" + name + "," + type.toPrologString() + ",[" + argsString + "]," + body.toPrologString() + ")";
    }

    public String toString() {
        String argsString = "";
        for (AstArg arg : args) {
            argsString += arg.toString() + ",";
        }
        return "funrec " + name + ":" + type.toString() + "(" + argsString + ") = " + body.toString() + ";";
    }
}
