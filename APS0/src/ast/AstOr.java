package ast;

public class AstOr implements Ast {
    public Ast left;
    public Ast right;

    public AstOr(Ast left, Ast right) {
        this.left = left;
        this.right = right;
    }

    public String toPrologString() {
        return "or(" + left.toPrologString() + "," + right.toPrologString() + ")";
    }

    public String toString() {
        return left.toString() + " || " + right.toString();
    }
}
