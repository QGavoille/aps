package ast;

import java.util.ArrayList;

public class AstTypeFun implements AstType {
    public ArrayList<AstType> arg;
    public AstType ret;

    public AstTypeFun(ArrayList<AstType> arg, AstType ret) {
        this.arg = arg;
        this.ret = ret;
    }

    public String toPrologString() {
        String argString = "";
        for (AstType arg : arg) {
            argString += arg.toPrologString() + ",";
        }
        return "fun([" + argString + "]," + ret.toPrologString() + ")";
    }

    public String toString() {
        String argString = "";
        for (AstType arg : arg) {
            argString += arg.toString() + ",";
        }
        return "[" + argString + "] -> " + ret.toString() + ";";
    }
}
