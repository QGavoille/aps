package ast;

public class AstTypeInt implements AstType {
    int value;
    public AstTypeInt() {
    }

    public String toPrologString() {
        return "int";
    }

    public String toString() {
        return "int";
    }
}
