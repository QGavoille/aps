package ast;

public class AstTypeBool implements AstType {
    boolean value;

    public AstTypeBool() {
    }

    public String toPrologString() {
        return "bool";
    }

    public String toString() {
        return "bool";
    }
}
