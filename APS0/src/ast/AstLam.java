package ast;

import java.util.ArrayList;

public class AstLam implements Ast {
    public ArrayList<AstArg> arg;
    public Ast body;

    public AstLam(ArrayList<AstArg> arg, Ast body) {
        this.arg = arg;
        this.body = body;
    }

    public String toPrologString() {
        String argString = "";
        for (AstArg arg : arg) {
            argString += arg.toPrologString() + ",";
        }
        return "lam([" + argString + "]," + body.toPrologString() + ")";
    }

    public String toString() {
        String argString = "";
        for (AstArg arg : arg) {
            argString += arg.toString() + ",";
        }
        return "[" + argString + "] -> " + body.toString() + ";";
    }
}
