package ast;

public class AstIf implements Ast {
    public Ast cond;
    public Ast then;
    public Ast els;

    public AstIf(Ast cond, Ast then, Ast els) {
        this.cond = cond;
        this.then = then;
        this.els = els;
    }

    public String toPrologString() {
        return "if(" + cond.toPrologString() + "," + then.toPrologString() + "," + els.toPrologString() + ")";
    }

    public String toString() {
        return "if " + cond.toString() + " then " + then.toString() + " else " + els.toString() + ";";
    }
}
