package ast;

public class AstConst implements Ast {

    String ident;
    AstType type;
    Ast val;

    public AstConst(String ident, AstType type, Ast val) {
        this.ident = ident;
        this.type = type;
        this.val = val;
    }

    public String toPrologString() {
        return "const(" + ident + "," + type.toPrologString() + "," + val.toPrologString() + ")";
    }

    public String toString() {
        return "const " + ident + ":" + type.toString() + " = " + val.toString() + ";";
    }

}

