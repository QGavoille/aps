package ast;

public class AstArg implements Ast {
    public String name;
    public AstType type;

    public AstArg(String name, AstType type) {
        this.name = name;
        this.type = type;
    }

    public String toPrologString() {
        return "arg(" + name + "," + type.toPrologString() + ")";
    }

    public String toString() {
        return name + ":" + type.toString();
    }
}
