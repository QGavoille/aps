package ast;

public class AstAnd implements Ast {
    public Ast left;
    public Ast right;

    public AstAnd(Ast left, Ast right) {
        this.left = left;
        this.right = right;
    }

    public String toPrologString() {
        return "and(" + left.toPrologString() + "," + right.toPrologString() + ")";
    }

    public String toString() {
        return "(" + left.toString() + " && " + right.toString() + ")";
    }

}
