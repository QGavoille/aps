package ast;

import java.util.ArrayList;

public class AstFun implements Ast {
    public String name;
    public AstType type;
    public ArrayList<AstArg> args;
    public Ast body;

    public AstFun(String name, AstType type, ArrayList<AstArg> args, Ast body) {
        this.name = name;
        this.type = type;
        this.args = args;
        this.body = body;
    }

    public String toPrologString() {
        String argsString = "";
        for (AstArg arg : args) {
            argsString += arg.toPrologString() + ",";
        }
        return "function(" + name + "," + type.toPrologString() + ",[" + argsString + "]," + body.toPrologString() + ")";
    }

    public String toString() {
        String argsString = "";
        for (AstArg arg : args) {
            argsString += arg.toString() + ",";
        }
        return "fun " + name + ":" + type.toString() + "(" + argsString + ") = " + body.toString() + ";";
    }
}
